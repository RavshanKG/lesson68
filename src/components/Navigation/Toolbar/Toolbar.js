import React from 'react';
import './Toolbar.css';
import {NavLink} from "react-router-dom";

const Toolbar = () => (
  <header className="Toolbar">
    <NavLink className="Toolbar-Item" activeClassName="Selected"
             to='/' exact>Counter</NavLink>
    <NavLink className="Toolbar-Item" activeClassName="Selected"
             to='/todo'>ToDo List</NavLink>
  </header>
);

export default Toolbar;