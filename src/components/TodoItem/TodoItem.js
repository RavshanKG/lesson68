import React, {Component} from 'react';
import './TodoItem.css';

class TodoItem extends Component {
  shouldComponentUpdate(newProps) {
    return (newProps.item.value !== this.props.item.value);
  }

  render() {
    return (
      <div className="TodoItem">
        <span className="TodoRemove" onClick={() => this.props.clicked(this.props.item.id)}>X</span>
        {this.props.item.value}
      </div>
    );
  }
}

export default TodoItem;