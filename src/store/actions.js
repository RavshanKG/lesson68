import axios from '../axios-counter';

export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';
export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_ERROR = 'FETCH_COUNTER_ERROR';
export const HANDLE_NEW_TODO = 'HANDLE_NEW_TODO';
export const POST_NEW_TODO = 'POST_NEW_TODO';
export const GET_TODO_REQUEST = 'GET_TODO_REQUEST';
export const GET_TODO_SUCCESS = 'GET_TODO_SUCCESS';
export const GET_TODO_ERROR = 'GET_TODO_ERROR';

export const incrementCounter = () => {
  return dispatch => {
    dispatch({ type: INCREMENT });
    dispatch(sendCounter());
  };
};

export const decrementCounter = () => {
  return dispatch => {
    dispatch({ type: DECREMENT });
    dispatch(sendCounter());
  };
};

export const addCounter = (amount) => {
  return dispatch => {
    dispatch({ type: ADD, amount });
    dispatch(sendCounter());
  };
};

export const subtractCounter = (amount) => {
  return dispatch => {
    dispatch({ type: SUBTRACT, amount });
    dispatch(sendCounter());
  };
};

export const fetchCounterRequest = () => {
  return {type: FETCH_COUNTER_REQUEST};
};

export const fetchCounterSuccess = counter => {
  return {type: FETCH_COUNTER_SUCCESS, counter: counter};
};

export const fetchCounterError = () => {
  return {type: FETCH_COUNTER_ERROR};
};

export const fetchCounter = () => {
  return dispatch => {
    dispatch(fetchCounterRequest());
    axios.get('/counter.json').then(response => {
      dispatch(fetchCounterSuccess(response.data));
    }, error => {
      dispatch(fetchCounterError(error));
    });
  }
};

export const sendCounter = () => {
  return ((dispatch, getState) => {
    dispatch(fetchCounterRequest());
    axios.put('/counter.json', getState().counter).then(response => {
      dispatch(fetchCounterSuccess(response.data));
    }, error => {
      dispatch(fetchCounterError(error));
    });
  })
};

export const newTodoHandler = event => {
  const newTodo = {value: event.target.value};
  return {type: HANDLE_NEW_TODO, newTodo}
};

export const getTodoRequest = () => {
  return {type: GET_TODO_REQUEST};
};

export const getTodoSuccess = todoList => {
  return {type: GET_TODO_SUCCESS, todoList: todoList};
};

export const postTodoSuccess = () => {
  return {type: POST_NEW_TODO};
};

export const getTodoError = () => {
  return {type: GET_TODO_ERROR};
};

export const getTodoItems = () => {
  return dispatch => {
    dispatch(getTodoRequest());
    axios.get('/todo.json').then(response => {
      let todoList = [];
      if (response.data) {
        todoList = Object.keys(response.data).map(item => {
          return {value: response.data[item].value, id: item};
        });
      }
      dispatch(getTodoSuccess(todoList));
    }, error => {
      dispatch(getTodoError(error));
    });
  }
};

export const postTodoItem = () => {
  return ((dispatch, getState) => {
    dispatch(getTodoRequest());
    axios.post('/todo.json', getState().newTodo).then(() => {
      dispatch(postTodoSuccess());
      dispatch(getTodoItems());
    }, error => {
      dispatch(getTodoError(error));
    });
  })
};

export const removeTodoItem = (id) => {
  return (dispatch => {
    dispatch(getTodoRequest());
    axios.delete(`/todo/${id}.json`).then(() => {
      dispatch(getTodoItems());
    }, error => {
      dispatch(getTodoError(error));
    });
  });
};