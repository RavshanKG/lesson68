import {
  INCREMENT, DECREMENT, ADD, SUBTRACT, FETCH_COUNTER_SUCCESS, GET_TODO_SUCCESS, HANDLE_NEW_TODO,
  POST_NEW_TODO, GET_TODO_ERROR, GET_TODO_REQUEST
} from "./actions";

const initialState = {
  counter: 0,
  todoList: [],
  newTodo: {
    value: ''
  },
  loading: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return {...state, counter: state.counter + 1};
    case DECREMENT:
      return {...state, counter: state.counter - 1};
    case ADD:
      return {...state, counter: state.counter + action.amount};
    case SUBTRACT:
      return {...state, counter: state.counter - action.amount};
    case FETCH_COUNTER_SUCCESS:
      return {...state, counter: action.counter};
    case HANDLE_NEW_TODO:
      return {...state, newTodo: action.newTodo};
    case GET_TODO_REQUEST:
      return {...state, loading: true};
    case GET_TODO_SUCCESS:
      return {...state, todoList: action.todoList, loading: false};
    case GET_TODO_ERROR:
      return {...state, loading: false};
    case POST_NEW_TODO:
      const newTodo = {value: ''};
      return {...state, newTodo};
    default:
      return state;
  }
};

export default reducer;