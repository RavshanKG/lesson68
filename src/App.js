import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";

import Layout from "./components/Layout/Layout";
import Counter from "./containers/Counter/Counter";
import TodoList from "./containers/TodoList/TodoList";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={Counter} />
          <Route path="/todo" component={TodoList} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
